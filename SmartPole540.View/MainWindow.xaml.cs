﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using SmartPole.View;
using SmartPole.View.Square;
using SmartPole.ViewModel;
using SmartPole.ViewModel.Interfaces;
using SmartPole540.ViewModel;
using SoltaLabs.Avalon.Core.Utilities;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SmartPole540.View
{
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : IMainWindow
   {
      public MainWindow()
      {
         InitializeComponent();

         var mainVm = new MainViewModel(this);
         DataContext = mainVm;

         ContentRendered += MainWindow_ContentRendered;
         Loaded += (sender, e) => mainVm.BusStopViewModel.Start();
         Closing += (sender, e) => mainVm.BusStopViewModel.Close();

         KeyDown += MainWindow_KeyDown;

         Closing += MainWindow_Closing;
      }

      private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
      {
         if(cameraControl.IsCapturing)
            cameraControl.StopCapture();
      }

      private void MainWindow_KeyDown(object sender, KeyEventArgs e)
      {
         Window w = sender as Window;
         if (w != null)
         {
            if (e.Key == Key.F11 && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
               if (FullScreenManager.GetIsFullScreen(w))
               {
                  w.ExitFullScreen();
               }
               else
               {
                  w.GoFullscreen();
               }

               return;
            }
         }

         OnKeyDown(e);
      }

      private void MainWindow_ContentRendered(object sender, EventArgs e)
      {
         var allDevices = cameraControl.GetVideoCaptureDevices().ToArray();

         if(allDevices.Length > 0)
            cameraControl.StartCapture(allDevices[0]);
         //webcam = new SimpleWebcam();
         //webcam.InitializeWebCam(ref imgVideo);
         //webcam.ResolutionSetting();
         // webcam.Start();
      }

      public void ToggleEmergency()
      {
         EmergencyPopup.IsOpen = !EmergencyPopup.IsOpen;
      }

      public void ToggleFeedback()
      {
         //TODO add feedback handler
      }

      private void Label_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
         ToggleEmergency();
      }

      private void Window_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
         CloseEmergencyPopup();
      }

      private void CloseEmergencyPopup()
      {
         if (!EmergencyPopup.IsMouseOver && !EmergencyButton.IsMouseOver && EmergencyPopup.IsOpen)
            ToggleEmergency();
      }
   
      public ISquareView GetSquareView()
      {
         return theRollPanel.BottomContent as SquareView;
      }

      public IHasBrowser GetJourneyView()
      {
         return theRollPanel.BodyContent as JourneyView;
      }
   }
}
