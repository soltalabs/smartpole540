﻿using Microsoft.VisualBasic.ApplicationServices;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole540
{
   public class SmartPoleApp : WindowsFormsApplicationBase
   {
      private App _mainApp;
      private KbHookManager _kbManager;

      public SmartPoleApp()
      {
         IsSingleInstance = true;

         //init log 
         SimpleLog.SetLogFile(".\\Log", "SmartPoleLog_", writeText: false);
         SimpleLog.StartLogging();

         UnhandledException += SmartPoleApp_UnhandledException;
         //more initiation here
      }

      protected override bool OnStartup(StartupEventArgs eventArgs)
      {
         _mainApp = new App();

         //    _kbManager = new KbHookManager();
         _mainApp.Exit += MainApp_Exit;
         _mainApp.DispatcherUnhandledException += MainApp_DispatcherUnhandledException;
         _mainApp.Run();
         return false;
      }

      private void SmartPoleApp_UnhandledException(object sender, UnhandledExceptionEventArgs e)
      {
         System.Windows.MessageBox.Show(e.Exception.Message);
         SimpleLog.Error(e.Exception.ToString());
         e.ExitApplication = false;
      }

      private void MainApp_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
      {
         System.Windows.MessageBox.Show(e.Exception.Message);
         SimpleLog.Error(e.Exception.ToString());
         e.Handled = true;
      }

      private void MainApp_Exit(object sender, System.Windows.ExitEventArgs e)
      {
         SimpleLog.StopLogging();
         //throw new NotImplementedException();
      }

      protected override bool OnUnhandledException(UnhandledExceptionEventArgs e)
      {
         System.Windows.MessageBox.Show(e.Exception.Message);
         e.ExitApplication = false;
         SimpleLog.Error(e.Exception.ToString());
         return true;
      }
   }
}
