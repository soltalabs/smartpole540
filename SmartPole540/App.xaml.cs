﻿using System;
using System.Windows;
using SmartPole540.View;

namespace SmartPole540
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            InitResources();
        }
      
        protected override void OnStartup(StartupEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
  //          mainWindow.GoFullscreen();
            base.OnStartup(e);
        }

        private void InitResources()
        {
            Resources = new ResourceDictionary();
            Resources.MergedDictionaries.Add(
                new ResourceDictionary
                {
                    Source = new Uri("/SmartPole.View;component/Assets/View.Resources.xaml", UriKind.RelativeOrAbsolute),
                });

            Resources.MergedDictionaries.Add(
                new ResourceDictionary
                {
                    Source = new Uri("/SoltaLabs.Avalon.View.Core;component/SoltaLabs.View.Core.Resources.xaml", UriKind.RelativeOrAbsolute),
                });
        }
    }
}
