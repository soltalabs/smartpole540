﻿using System;
using SmartPole;

namespace SmartPole540
{
    public class ProcessEntry
    {
        /// <summary>
        /// Application initial point
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        public static void Main(string[] args)
        {
            SmartPoleApp theApp = new SmartPoleApp();
            theApp.Run(args);
        }
    }
}
