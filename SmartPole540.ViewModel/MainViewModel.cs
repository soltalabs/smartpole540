﻿using System.Windows.Input;
using SmartPole.ViewModel;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.Core.Utilities;

namespace SmartPole540.ViewModel
{
   public class MainViewModel : ObservableObject<MainViewModel>
   {
      private readonly IMainWindow _window;
      public MainViewModel(IMainWindow window)
      {
         _window = window;
         EmergencyCommand = new DelegateCommand(() => _window.ToggleEmergency());
         SquareViewModel = new SquareViewModel(_window.GetSquareView());
         JourneyViewModel = new WebViewModel(_window.GetJourneyView());
      }
      public BusStopViewModel BusStopViewModel { get; set; } = new BusStopViewModel();

      public SquareViewModel SquareViewModel { get; set; }

      public WebViewModel JourneyViewModel { get; set; }

      public ICommand EmergencyCommand { get; set; }
   }
}
